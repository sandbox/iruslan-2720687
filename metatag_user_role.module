<?php

/**
 * @file
 *
 * Allows to manage via default metatags UI default metatags per user being viewed role.
 */

/**
 * Implements hook_metatag_config_instance_info_alter().
 *
 * @param $info
 */
function metatag_user_role_metatag_config_instance_info_alter(&$info) {
  // Allow to add default config per role.
  if (metatag_entity_supports_metatags('user')) {
    $roles = user_roles(TRUE);
    foreach ($roles as $rid => $role) {
      if ($rid != DRUPAL_AUTHENTICATED_RID) {
        $info['user:' . $role] = array('label' => $role);
      }
    }
  }
}

/**
 * Implements hook_metatag_get_entity_metatags_instance_alter().
 *
 * @param $instance
 * @param $entity
 * @param $entity_type
 * @param $bundle
 */
function metatag_user_role_metatag_get_entity_metatags_instance_alter(&$instance, $entity, $entity_type, $bundle) {
  // Extract user role with highest weight.
  if ($entity_type == 'user') {
    foreach (array_reverse(user_roles(), TRUE) as $rid => $role) {
      if ($rid == DRUPAL_AUTHENTICATED_RID) {
        continue;
      }

      if (isset($entity->roles[$rid])) {
        $instance = 'user:' . $role;
        break;
      }
    }
  }
}
